﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelTemplates
{
    class Program
    {
        private static Dictionary<string, Dictionary<string, string>> fonts = new Dictionary<string, Dictionary<string, string>>();
        private static Dictionary<string, Dictionary<string, string>> specialFonts = new Dictionary<string, Dictionary<string, string>>();
        private static Dictionary<string, Dictionary<string, string>> currency = new Dictionary<string, Dictionary<string, string>>();
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the path to the folder: ");
            string sourceFolder = Console.ReadLine();
            if (Directory.Exists(sourceFolder))
                using (ExcelHandler xlHandler = new ExcelHandler())
                {
                    LoadSettings(xlHandler);                
                    foreach(string subfolder in Directory.GetDirectories(sourceFolder))
                    {
                        string langCode = Path.GetFileNameWithoutExtension(subfolder);
                        var commands = CreateCommand(langCode, "font", "currency", "unhideandunprotect");

                        if (!commands.IsActive())
                            continue; 

                        foreach (string file in Directory.GetFiles(subfolder))
                        {
                            if(Path.GetExtension(file).Contains(".xl"))
                            {
                                xlHandler.OpenBook(file);
                                if (xlHandler.TryCallChangesInSheets(commands) && xlHandler.TryCallChangesInStyles(commands))
                                    Console.WriteLine($"{file} - processed");
                                else
                                    Console.WriteLine($"{file} - error");
                                xlHandler.CloseWorkbook(true);
                            }
                        }
                    }
                }
            Console.ReadLine();
        }

        static void LoadSettings(ExcelHandler xlHandler)
        {
            try
            {
                string dataPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase) + "\\Datasheet.xlsx";
                xlHandler.OpenBook(dataPath);

                xlHandler.SelectSheet("Fonts");
                xlHandler.SelectAll();
                fonts = xlHandler.RangeToDictionary(true);

                xlHandler.SelectSheet("Special Langs");
                xlHandler.SelectAll();
                specialFonts = xlHandler.RangeToDictionary(true);

                xlHandler.SelectSheet("Currency");
                xlHandler.SelectAll();
                currency = xlHandler.RangeToDictionary(true);

                xlHandler.CloseWorkbook(false);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error reading");
            }
        }

        static CommandBridge CreateCommand(string langCode, params string[] commands)
        {
            CommandBridge commandBridge = new CommandBridge();

            foreach(string command in commands)
                switch(command)
                {
                    case "font":
                        if (specialFonts.Keys.Contains<string>(langCode))
                        {
                            commandBridge.SpecialFont = true;
                            commandBridge.FontsData = specialFonts[langCode]["Font"];
                        }
                        else if (fonts.Keys.Contains<string>(langCode))
                        {
                            commandBridge.Fonts = true;
                            commandBridge.FontsData = fonts[langCode];
                        }
                        break;

                    case "currency":
                        if (currency.Keys.Contains<string>(langCode))
                        {
                            commandBridge.Currency = true;
                            commandBridge.CurrencyData = currency[langCode];
                        }
                        break;

                    case "unhideandunprotect":
                        commandBridge.UnhideAndUnprotect = true;
                        break;
                }
            return commandBridge;
        }
    }
}
