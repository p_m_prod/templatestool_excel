﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using XL = Microsoft.Office.Interop.Excel;

namespace ExcelTemplates
{
    /// <summary>
    /// Represents the collection of useful methods to simplify the work with MS Excel.
    /// </summary> 
    class ExcelHandler : IDisposable
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of ExcelHandler class
        /// </summary>
        public ExcelHandler()
        {
            xlApp = new XL.Application();
            xlApp.Visible = false;
        }
        /// <summary>
        /// Initializes a new instance of ExcelHandler class and opens specified file.
        /// </summary>
        public ExcelHandler(string path)
        {            
            xlApp = new XL.Application();
            xlApp.Visible = false;
            OpenBook(path);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Opens specified file.
        /// </summary>
        /// <param name="path">The path to the file.</param>
        /// <remarks>Throws exception if specified file doesn't exist or is not a valid MS Excel document.</remarks>
        public void OpenBook(string path)
        {
            if (!File.Exists(path))
                throw new Exception("File doesn't exist.");

            if (!Path.GetExtension(path).Contains(".xl"))
                throw new Exception("Please select valid Excel document.");

            xlBook = xlApp.Workbooks.Open(path, misValue, false, misValue, misValue, misValue, true, misValue, misValue, true, misValue, misValue, misValue, misValue, misValue);
        }
        /// <summary>
        /// Creates new workbook.
        /// </summary>
        public void CreateNewBook()
        {
            xlBook = xlApp.Workbooks.Add();
        }
        /// <summary>
        /// Creates new worksheet.
        /// </summary>
        /// <remarks>Will create new workbook, if there isn't a workbook open in the application.</remarks>
        public void CreateNewSheet()
        {
            if (xlBook == null)
                CreateNewBook();
            xlSheet = xlBook.Worksheets.Add();
        }
        /// <summary>
        /// Creates new worksheet with specified name.
        /// </summary>
        /// <param name="name">Name of the worksheet.</param>
        /// <remarks>Will create new workbook, if there isn't a workbook open in the application.</remarks>
        public void CreateNewSheet(string name)
        {
            if (xlBook == null)
                CreateNewBook();
            xlSheet = xlBook.Worksheets.Add();
            xlSheet.Name = name;
        }
        /// <summary>
        /// Selects a worksheet
        /// </summary>
        /// <param name="name">The name of the worksheet.</param>
        /// <remarks>Throws exception if there is no active workbook or the active workbook doesn't contain specified worksheet.</remarks>
        public void SelectSheet(string name)
        {
            if (xlBook == null)
                throw new Exception("Trying to select a sheet w/o active workbook.");

            bool found = false;

            foreach (XL.Worksheet sheet in xlBook.Sheets)
                if (name.Equals(sheet.Name))
                {
                    xlSheet = sheet;
                    found = true;
                    break;
                }

            if (!found)
                throw new Exception("Active workbook doesn't contain a sheet with specified name.");
        }
        /// <summary>
        /// Selects specified cell. 
        /// </summary>
        /// <param name="start">Cell address i.e. A1, E45 etc.</param>
        /// <remarks>Throws exception if there is no active worksheet or if the address provided is not valid.</remarks>
        public void SelectRange(string start)
        {
            if (xlSheet == null)
                throw new Exception("Trying to select a range w/o active worksheet.");

            if(!_IsValidRange(start))
                throw new Exception("Trying to select a range - invalid address provided.");

            xlRange = xlSheet.get_Range(start);
        }
        /// <summary>
        /// Selects specified range. The range is specified by its starting and ending cells.
        /// </summary>
        /// <param name="start">The starting cell of the range.</param>
        /// <param name="end">The ending cell of the range.</param>
        /// <remarks>Throws exception if there is no active worksheet or if the address provided is not valid.</remarks>
        public void SelectRange(string start, string end)
        {
            if (xlSheet == null)
                throw new Exception("Trying to select a range w/o active worksheet.");

            if (!_IsValidRange(start) || !_IsValidRange(end))
                throw new Exception("Trying to select a range - invalid address provided.");

            xlRange = xlSheet.get_Range(start, end);
        }
        /// <summary>
        /// Selects all used cells in active worksheeet.
        /// </summary>
        /// <remarks>Throws exception if there is no active worksheet.</remarks>
        public void SelectAll()
        {
            if (xlSheet == null)
                throw new Exception("Trying to select a range w/o active worksheet.");

            xlRange = xlSheet.UsedRange;
        }
        /// <summary>
        /// Converts excel range to Dictionary object filled with values from the range.
        /// </summary>
        /// <param name="withTitle">Determines the type of returned dictionary. <see cref="ExcelHandler._RangeWithoutTitle"/> and <see cref="ExcelHandler._RangeWithTitle"/> for more information. Default false.</param>
        /// <returns>
        /// Type: System.Dynamic
        /// Determined by withTitle parameter.
        /// </returns>
        /// <remarks>Throws exception if there is no range selected or the selected range contains of less than 2 cells.</remarks>
        public dynamic RangeToDictionary(bool withTitle = false)
        {
            if (xlRange == null)
                throw new Exception("Can't convert range to dictionary - no range selected.");

            if (xlRange.Count < 2)
                throw new Exception("Can't convert range to dictionary - please select at least 2 cells");

            if(withTitle)
            {
                var toReturn = _RangeWithTitle();
                return toReturn;
            }
            else
            {
                var toReturn = _RangeWithoutTitle();
                return toReturn;
            }
        }       
        /// <summary>
        /// Tries to perform specified commands in all sheets in active workbook.
        /// </summary>
        /// <param name="command">CommandBridge object with specified commands.</param>
        /// <returns>
        /// Type: System.Boolean
        /// true if successful; false otherwise
        /// </returns>
        public bool TryCallChangesInSheets(CommandBridge command)
        {
            try
            {
                if (command.UnhideAndUnprotect)
                    UnprotectAndUnhide();

                foreach (XL.Worksheet sheet in xlBook.Worksheets)
                {
                    xlSheet = sheet;
                    SelectAll(); 
                    foreach (XL.Range cell in xlRange)
                    {
                        if(command.Fonts)                        
                            _ChangeFont(command.FontsData, cell);
                        else if (command.SpecialFont)
                            cell.Font.Name = command.FontsData;

                        if (command.Currency)
                            _RepairCurrency(command.CurrencyData, cell);
                    }                    
                }                

                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }
        /// <summary>
        /// Tries to perform specified commands in every style in active workbook.
        /// </summary>
        /// <param name="command">CommandBridge object with specified commands.</param>
        /// <returns>
        /// Type: System.Boolean
        /// true if successful; false otherwise
        /// </returns>
        public bool TryCallChangesInStyles(CommandBridge command)
        {
            try
            {
                foreach (XL.Style style in xlBook.Styles)
                {
                    if (command.Fonts)
                        _ChangeFont(command.FontsData, style);
                    else if (command.SpecialFont)
                        style.Font.Name = command.FontsData;

                    if (command.Currency)
                        _RepairCurrency(command.CurrencyData, style);
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        /// <summary>
        /// Unprotects and unhides every sheet in active workbook.
        /// </summary>
        /// <remarks>Throws exception if there is no active workbook.</remarks>
        public void UnprotectAndUnhide()
        {
            if (xlBook == null)
                throw new Exception("No active workbook.");

            foreach (XL.Worksheet sheet in xlBook.Sheets)
            {
                sheet.Visible = XL.XlSheetVisibility.xlSheetVisible;
                sheet.Unprotect();
            }                         
        }        
        /// <summary>
        /// Closes the active workbook.
        /// </summary>
        /// <param name="save">Determines whether the changes should be saved before closing.</param>
        /// <remarks>Throws exception if there is no active workbook.</remarks>
        public void CloseWorkbook(bool save)
        {
            if (xlBook == null)
                throw new Exception("Cannot close workbook - no workbook is open.");

            xlBook.Close(save);
            xlBook = null;

        }
        /// <summary>
        /// Dispose method. Releases all resources and closes the application.
        /// </summary>
        public void Dispose()
        {
            Marshal.ReleaseComObject(xlRange);
            GC.WaitForPendingFinalizers();
            Marshal.ReleaseComObject(xlSheet);
            GC.WaitForPendingFinalizers();
            if (xlBook != null)
            {
                CloseWorkbook(false);
                Marshal.ReleaseComObject(xlBook);
                GC.WaitForPendingFinalizers();
            }
            GC.Collect();
            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Determines whethes the specified string is valid MS Excel address.
        /// </summary>
        /// <param name="rangeRef">Specified address.</param>
        /// <returns>
        /// Type: System.Boolean
        /// true if the address is valid; false otherwise
        /// </returns>
        private bool _IsValidRange(string rangeRef)
        {
            try
            {
                var x = xlRange.get_Range(rangeRef);
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Creates Dictionary from selected range. The keys in dictionary are cell addresses and the values are cell values.
        /// </summary>
        /// <returns>
        /// Type: System.Collections.Generic.Dictionary
        /// </returns>
        private Dictionary<string, string> _RangeWithoutTitle()
        {
            Dictionary<string, string> returnedDictionary = new Dictionary<string, string>();

            foreach (XL.Range cell in xlRange)
            {
                string cellAddress = $"{_ExcelColumnIndexToName(cell.Column)}{cell.Row}";
                returnedDictionary.Add(cellAddress, cell.Text);
            }

            return returnedDictionary;
        }
        /// <summary>
        /// Creates three dimensional Dictionary from selected range. The keys in dictionary are row headers and the values are dictionaries created from column headers and respective cell values.
        /// </summary>
        /// <returns>
        /// Type: System.Collections.Generic.Dictionary
        /// </returns>
        private Dictionary<string, Dictionary<string, string>> _RangeWithTitle()
        {
            var returnedDictionary = new Dictionary<string, Dictionary<string, string>>();

            for (int r = 2; r <= xlRange.Rows.Count; r++)
            {
                string rowTag = xlRange.Rows[r].Columns[1].Text;
                if (!String.IsNullOrWhiteSpace(rowTag))
                {
                    var rowDictionary = new Dictionary<string, string>();
                    for (int c = 2; c <= xlRange.Rows[r].Columns.Count; c++)
                    {
                        if (!String.IsNullOrWhiteSpace(xlRange.Rows[r].Columns[c].Text))
                        {
                            var valueTag = xlRange.Rows[1].Columns[c].Text;
                            var value = xlRange.Rows[r].Columns[c].Text;
                            rowDictionary.Add(valueTag, value);
                        }
                    }
                    returnedDictionary.Add(rowTag, rowDictionary);
                }
            }

            return returnedDictionary;
        }
        /// <summary>
        /// Determines the name of the column from specified index.
        /// </summary>
        /// <param name="index">The index of the column.</param>
        /// <returns>
        /// Type: System.String
        /// The name of the column.
        /// </returns>
        private string _ExcelColumnIndexToName(int index)
        {
            string range = string.Empty;
            if (index < 1) return range;
            int a = 26;
            int x = (int)Math.Floor(Math.Log((index) * (a - 1) / a + 1, a));
            index -= (int)(Math.Pow(a, x) - 1) * a / (a - 1);
            for (int i = x + 1; index + i > 0; i--)
            {
                range = ((char)(65 + index % a)).ToString() + range;
                index /= a;
            }
            return range;
        }
        /// <summary>
        /// Changes font of the specified object.
        /// </summary>
        /// <param name="fontValues">Dictionary containing name of fonts and their specified replacement.</param>
        /// <param name="obj">Object with Font property.</param>
        private void _ChangeFont(Dictionary<string, string> fontValues, dynamic obj)
        {
            if (fontValues.ContainsKey(obj.Font.Name))
                obj.Font.Name = fontValues[obj.Font.Name];
        }
        /// <summary>
        /// Determines if NumberFormat property of specified object is representation of Currency format. If true, sets it to specified value. 
        /// </summary>
        /// <param name="currencyString">String to replace current NumberFormat.</param>
        /// <param name="obj">Specified object with NumberFormat property.</param>
        private void _RepairCurrency(string currencyString, dynamic obj)
        {
            bool isRange = obj is XL.Range;
            if (new Regex("\"\\$\".*#.*##0").IsMatch(obj.NumberFormat))
            {
                string defText = (isRange) ? obj.Text.ToString() : null;
                obj.NumberFormat = currencyString;
                if(isRange)
                    if (new Regex("^#+").IsMatch(obj.Text) && !defText.Equals(obj.Text))
                        xlRange.Columns[obj.Column].AutoFit();
            }
        }
        #endregion

        #region Fields
        private System.Reflection.Missing misValue = System.Reflection.Missing.Value;
        private XL.Application xlApp { get; set; }
        private XL.Workbook xlBook { get; set; }
        private XL.Worksheet xlSheet { get; set; }
        private XL.Range xlRange { get; set; }
        #endregion
    }
}
