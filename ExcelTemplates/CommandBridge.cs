﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelTemplates
{
    /// <summary>
    /// Represents the collection of commands performed by ExcelHandler class.
    /// </summary>
    /// <remarks>
    /// Font and SpecialFont properties are either both false or opposite to each other. 
    /// If you want to add a type of command to CommandBridge just create new property/ies. Don't forget to add the property check to IsActive() method.
    /// </remarks>
    class CommandBridge
    {
        /// <summary>
        /// Initializes new instance of CommandBridge. 
        /// </summary>
        /// <remarks>
        /// The instance will start with no commands active. This is for clarity in code. 
        /// If you want to change this, please override the constructor.
        /// </remarks>
        public CommandBridge()
        {
            Fonts = false;
            SpecialFont = false;
            Currency = false;
            UnhideAndUnprotect = false;
        }

        #region Commands
        public bool Fonts
        {
            get
            {
                return Fonts;
            }
            set
            {
                Fonts = value;
                SpecialFont = !value;
            }
        }
        public bool SpecialFont
        {
            get
            {
                return SpecialFont;
            }
            set
            {
                Fonts = !value;
                SpecialFont = value;
            }
        }
        public bool Currency { get; set;}
        public bool UnhideAndUnprotect { get; set; }
        #endregion

        #region CommandData
        public dynamic FontsData { get; set; }
        public dynamic CurrencyData { get; set; }
        #endregion

        /// <summary>
        /// Check if there is any active command in CommandBridge object.
        /// </summary>
        /// <returns>
        /// Type: System.Boolean
        /// true if there is at least one active command in this CommandBridge;
        /// otherwise; false
        /// </returns>
        public bool IsActive()
        {
            if (!UnhideAndUnprotect)
                if (!Fonts && !SpecialFont)
                    if (!Currency)
                        return false;

            return true;
        }
    }
}
